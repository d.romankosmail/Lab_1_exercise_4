#include "stdafx.h"
#include <iostream>
#include <cmath>
using namespace std;

double fact(double n)
{
	if (n < 0) // ïåðåâ³ðêà íà â³ä'ºìíå ÷èñëî
		return 0;
	if (n == 0) // ïåðåâ³ðêà íà 0
		return 1;
	else
		return n * fact(n - 1); // ðåêóðñ³ÿ
}

void cinFail()
{
	if (cin.fail())
	{
		cout << "íåêîðåêòíå ââåäåííÿ!" << endl;
		system("pause");
		return;
	}
}
void func1()
{
	double n, k=1;
	cout << "ââåä³òü n" << endl; cin >> n;
	cinFail();
	for (k = 1; k <= n; k++)
	{
		cout << (fact(2*k)) / (fact(pow(k, 2))) << endl;
	}
}

void func2()
{
	double n, i = 1;
	cout << "ââåä³òü n" << endl; cin >> n;
	cinFail();
	for (i = 1; i <= n; i++)
	{
		cout << (1 / fact(n))*(pow(i + 1, 3)) / (fact(i) - pow(i, n)) << endl;
	}
}
int main()
{
	cout.precision(4);
	setlocale(0, "ukr");
	double x, y;
	int choose;
	cout << "âèáåð³òü çàäà÷ó (1 àáî 2)" << endl; cin >> choose;
	cinFail();
	switch (choose)
	{
	case 1: func1(); break;
	case 2: func2(); break;
	default: return 0;
	}
	system("pause");
	return 0;
}
